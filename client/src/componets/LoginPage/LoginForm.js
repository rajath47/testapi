import React, { Component } from 'react';
import { Panel, Form, FormGroup, FormControl, Button } from 'react-bootstrap';
import '../LoginPage/LoginPage.css';






const divStyle = {
  display: 'flex',
  alignItems: 'center',
  marginTop: -100
};

const panelStyle = {
  backgroundColor: 'rgba(255,255,255,0.5)',
  border: 0,
  paddingLeft: 20,
  paddingRight: 20,
  width: 300,
};

const buttonStyle = {
  marginBottom: 0
};


const forgot = {
  marginleft: 10,
};


//////////////////////////////////////


class LoginForm extends Component {

  handleChange = e => {
    this.form.validateFields(e.target);
  }

  contactSubmit = e => {
    e.preventDefault();

    this.form.validateFields();

    if (!this.form.isValid()) {
      console.log('form is invalid: do not submit');
    } else {
      console.log('form is valid: submit');
   } }
  
  render() {
    return (


      <div style={divStyle}>
        <Panel style={panelStyle}>

        
 <FormGroup controlId="" onClick='this.con()'>
             <h6 className="headder">Sign in</h6>
            </FormGroup>
          <Form horizontal className="LoginForm" id="loginForm">
            <FormGroup controlId="formEmail">
              <FormControl type="text" placeholder="Email Address"  required onChange={this.handleChange}
                 />
            </FormGroup>
            <FormGroup controlId="formPassword">
              <FormControl type="password" placeholder="Password" />
            </FormGroup>

 <FormGroup controlId="" onClick='this.con()'>
             <h6 className="forgot" onClick=''>Forgot password?</h6>
            </FormGroup>
            
            
            <FormGroup style={buttonStyle} controlId="formSubmit">
              <Button bsStyle="primary" type="submit" onClick={this.handleFormSubmit} className="login">
                Login
              </Button>
            </FormGroup>
          </Form>
        </Panel>
      </div>
    )
  }
}

export default LoginForm;
