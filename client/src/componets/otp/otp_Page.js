import React, { Component } from 'react';
import OtpForm from './otp_Form';

import '../LoginPage/LoginPage.css';

class OtpPage extends Component {
  render() {
    return (
      <div className="LoginPage">
        <OtpForm />
      </div>
    );
  }
}

export default OtpPage;
