import React, { Component } from 'react';
import Reset_form from '../reset/reset_form';

import '../reset/reset.css';

class ResetPage extends Component {
  render() {
    return (
      <div className="LoginPage">
        <Reset_form />
      </div>
    );
  }
}

export default ResetPage;
