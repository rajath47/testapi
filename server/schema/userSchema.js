const mongoose = require('mongoose');
// const Joi = require('joi');
// const bcrypt = require('bcrypt')

//Schema of User
 const User = mongoose.model('User',new mongoose.Schema({
     name:String,
     username:String,
     email:{
         type:String,
         required:true,
         minlength:5,
         maxlength:255,
         unique:true
     },
     password:{
        type:String,
        required:true,
        minlength:5,
        maxlength:1024
     },
     role:String,
     phno:{
         type:Number,
         minlength:10,
         maxlength:10
     },
       tokens:{
        token:String,
         tokenStart:Boolean,
         tokenExp:Date         
     },
       lastOnline:{type:Date,default:Date.now()},
        logcount:Number,
         resetpassword:{
           otp:Number,
           status:Boolean,
          expiry:Date
        }
 }));
 

 //exporting model
exports.User = User;


