const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const bcrypt = require('bcrypt')
var rn = require('random-number');
var options = {
    min: 1000
    , max: 10000
    , integer: true
}

const User = require('../schema/userSchema.js');

mongoose.connect('mongodb://rajath47:nagarajath7@ds121382.mlab.com:21382/testapi', { useNewUrlParser: true });
mongoose.connection
    .once('open', () => console.log('CONNECTED'))
    .on('error', (err) => {

        console.log('Could not connect', err)
    });

mongoose.Promise = global.Promise;
// fetching data from db
app.get('/login', (req, res) => {
    User.find({}, ['username', 'password',]).then(data => {
        res.status(200).send(data);
    }).catch(err => {
        res.status(400).send(err)
        console.log("Error occurred");

    });
});

app.post('/login', (req, res) => {
    const newUser = new User({
        username: req.body.username,
        password: req.body.password,
    });
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) return err;
            newUser.password = hash;
            newUser.save().then(savedUser => {

                res.send('USER SAVED')

            }).catch(err => {

                res.status(404).send('USER NOT SAVE BECAUSE .....')

            });

        });
    });


});
app.post('/forgot', (req, res) => {
    var uid = new User();
    uid.tokens.token = Math.floor(Math.random() * 900000000300000000000) + 1000000000000000
    uid.username = req.body.username;
    uid.save().then(savedUser => {

        res.send('USER SAVED')

    }).catch(err => {

        res.status(404).send('USER NOT SAVE BECAUSE .....')
        console.log(err)

    });
})

//port listening
const port = 8080 || process.env.PORT;
app.listen(port, () => {
    console.log(`listening on port`, port);
});
