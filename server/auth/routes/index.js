var express = require('express')
var router = express.Router()
var crypto = require('crypto');
var async = require('async');
const Nexmo = require('nexmo');

var User = require('../models/users');


const nexmo = new Nexmo({
  apiKey: process.env.NEXMO_KEY,
  apiSecret: process.env.NEXMO_SCERET
});


var loggedin = (req, res, next) => {
  if (req.isAuthenticated()) {
    next()
  } else {
    res.redirect('/login')
  }
}

/* GET home page. */
router.get('/', loggedin, (req, res, next) => {
  res.send({
    msg: 'Successfuly loged in'
  })
})

router.get('/login', (req, res, next) => {
  res.status(401).send({
    msg: 'userName or password is incorect'
  })
})

router.get('/profile', loggedin, (req, res, next) => {
  res.send({
    user: req.user
  })
})

router.get('/logout', (req, res) => {

  req.logout()
  res.send({
    msg: 'logout successfully'
  })
  // res.redirect('/auth/login')
})


router.post('/reset', (req, res) => {
  // A user registers with a mobile phone number

  let email = req.body.email;
  console.log(email);
  User.findOne({
    email
  }).then(user => {
    if (!user) {
      return res.status(401).send({
        message: "Invalid Email id"
      })
    }
    nexmo.verify.request({
      number: '91' + user.phone_no,
      brand: 'VVCE, Mysuru,OTP '
    }, (err, result) => {
      if (err) {
        //res.sendStatus(500);
        res.status(500).send({
          message: 'Server Error'
        });
      } else {
        console.log(result);
        let requestId = result.request_id;
        if (result.status == '0') {
          res.send({
            message: "OTP sent to your mobile number, please cheack!",
            requestId
          });
        } else {
          res.status(401).send({
            message: result.error_text
          });
        }
      }
    });
  }).catch(err => res.status(400).send(err))

});

router.post('/verify', (req, res) => {
  // Checking to see if the code matches
  let pin = req.body.pin;
  let requestId = req.body.requestId;

  nexmo.verify.check({
    request_id: requestId,
    code: pin
  }, (err, result) => {
    if (err) {
      res.status(500).send(err);
    } else {
      console.log(result);
      // Error status code: https://docs.nexmo.com/verify/api-reference/api-reference#check
      if (result && result.status == '0') {
        res.status(200).send({
          message: 'OTP is verified successfully'
        });
      } else {
        res.status(401).send(result.error_text);
      }
    }
  });
});

module.exports = router