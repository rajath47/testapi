require('../config/config')

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');
var flash = require('express-flash');


require('./passport')(passport)
require('../db/mongoose')

var index = require('./routes/index');
var auth = require('./routes/auth')(passport);

var app = express()

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  secret: process.env.PASSPORT_SECRET,
  saveUninitialized: false,
  resave: false
}))
app.use(flash());

app.use(passport.initialize())
app.use(passport.session())

app.use('/', index);
app.use('/auth', auth)


app.listen(process.env.PORT, () => {
  console.log(`server up at ${process.env.PORT}`)
})